from rest_framework import serializers
from . import models
from rest.task_list.models import TaskList
from rest.tags.models import Tags


class TasksSerializer(serializers.HyperlinkedModelSerializer):
    TaskList = serializers.PrimaryKeyRelatedField(queryset=TaskList.objects.all())
    Tags = serializers.PrimaryKeyRelatedField(many=True, queryset=Tags.objects.all())

    class Meta:
        model = models.Tasks
        fields = [
            "Id", "Title", "Notes", "Priority",
            "RemindMeOn", "ActivityType",
            "Status", "TaskList", "Tags"
        ]