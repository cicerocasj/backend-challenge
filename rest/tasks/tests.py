import json
from rest_framework.test import APIClient
from django.contrib.auth.models import User
from . import models, serializers
from rest_framework import status
from django.test import TestCase
from rest.task_list.models import TaskList
from rest.tags.models import Tags
import logging


log = logging.getLogger()


class TasksTests(TestCase):

    def get_token(self):
        response = self.client.post('/api-token-auth/', {'username': self.username, 'password': self.password})
        return response.json().get('token')

    def basic_task(self, number):
        task_list = TaskList.objects.create(Name='list 1')
        data = dict(
            Title='Title %s' % number,
            Notes='Notes %s' % number,
            Priority=1,
            RemindMeOn='2020-08-23',
            TaskList=task_list
        )
        return data

    def setUp(self):
        self.client = APIClient()
        self.MyModel = models.Tasks
        self.name = 'tasks'
        self.MySerializer = serializers.TasksSerializer
        self.username = 'api'
        self.password = 'backend-challenge2020'
        self.user = User.objects.create_superuser(username=self.username, password=self.password)
        self.token = self.get_token()
        self.assertIsInstance(self.token, str, 'token as string')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token)
        self.create_instance()

    def create_instance(self):
        self.tag1 = Tags.objects.create(Name='tag 1')
        self.tag2 = Tags.objects.create(Name='tag 2')
        self.tag3 = Tags.objects.create(Name='tag 3')
        self.instance1 = self.MyModel.objects.create(**self.basic_task(1))
        self.instance2 = self.MyModel.objects.create(**self.basic_task(2))
        self.instance3 = self.MyModel.objects.create(**self.basic_task(3))

    def tearDown(self):
        User.objects.all().delete()
        self.MyModel.objects.all().delete()

    def url(self, pk=None):
        return '/%s/%s/' % (self.name, pk) if pk else '/%s/' % self.name

    def test_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION='')
        response = self.client.get(self.url())
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED, 'not authorized')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token)
        response = self.client.get(self.url())
        self.assertEqual(response.status_code, status.HTTP_200_OK, 'authorized')

    def test_get_list(self):
        response = self.client.get(self.url())
        queryset = self.MyModel.objects.all()
        serializer = self.MySerializer(queryset, many=True)
        self.assertEqual(response.data, serializer.data, 'list content')
        self.assertEqual(response.status_code, status.HTTP_200_OK, 'status code GET')

    def test_get_instance(self):
        response = self.client.get(self.url(self.instance1.pk))
        serializer = self.MySerializer(self.instance1)
        self.assertEqual(response.data, serializer.data, 'content')
        self.assertEqual(response.status_code, status.HTTP_200_OK, 'status code GET')
        invalid_pk = '1234'
        response = self.client.get(self.url(invalid_pk))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND, 'status code GET')

    def test_post(self):
        n_elements_pre = self.MyModel.objects.all().count()
        data = self.basic_task(1)
        data['TaskList'] = str(data.get('TaskList').pk)
        data['Tags'] = [str(self.tag1.pk)]
        response = self.client.post(
            self.url(),
            data=json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, 'status code POST')
        n_elements_post = self.MyModel.objects.all().count()
        self.assertEqual(n_elements_pre + 1, n_elements_post, 'register count')

    def test_put(self):
        new_title = '%s 1.1' % self.name
        data = self.basic_task(1)
        data['TaskList'] = str(data.get('TaskList').pk)
        data['Tags'] = [str(self.tag1.pk)]
        data['Title'] = new_title

        response = self.client.put(
            self.url(self.instance1.pk),
            data=json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, 'status code PUT')
        updated_instance = self.MyModel.objects.get(pk=self.instance1.pk)
        self.assertEqual(updated_instance.Title, new_title, 'register field value')

    def test_delete(self):
        n_elements_pre = self.MyModel.objects.all().count()
        response = self.client.delete(
            self.url(self.instance1.pk)
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, 'status code DELETE')
        n_elements_post = self.MyModel.objects.all().count()
        self.assertEqual(n_elements_pre - 1, n_elements_post, 'register count')

    def test_patch(self):
        new_title = '%s 1.1' % self.name
        data = self.basic_task(1)
        data['TaskList'] = str(data.get('TaskList').pk)
        data['Tags'] = [str(self.tag1.pk)]
        data['Title'] = new_title
        response = self.client.put(
            self.url(self.instance1.pk),
            data=json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, 'status code PATCH')
        updated_instance = self.MyModel.objects.get(pk=self.instance1.pk)
        self.assertEqual(updated_instance.Title, new_title, 'register field value')

    def test_count_tags_after_tasks_save(self):
        log.info('Create relation between task and tag')
        self.instance1.Tags.add(self.tag1)
        self.assertEqual(
            Tags.objects.filter(pk=self.tag1.pk, Count=self.tag1.Count + 1).count(),
            1,
            'has 1 elemente, after filter with Count += 1'
        )
        log.debug('only debug for example')
        self.assertEqual(
            Tags.objects.filter(Count=0).count(),
            2,
            'Count change'
        )
        self.instance1.Tags.add(self.tag2)
        self.assertEqual(
            Tags.objects.filter(Count=0).count(),
            1,
            'Count change'
        )
        log.warning('Its a example for warning')
        self.instance2.Tags.add(self.tag3)
        self.assertEqual(
            Tags.objects.filter(Count=0).count(),
            0,
            'Count change'
        )
        log.error('Its a example for error')
        self.instance2.Tags.add(self.tag1)
        self.assertEqual(
            Tags.objects.filter(Count__gt=1).count(),
            1,
            'Count change'
        )
        self.instance2.Tags.remove(self.tag1)
        self.assertEqual(
            Tags.objects.filter(Count__gt=1).count(),
            0,
            'Count change'
        )
        try:
            raise Exception('Its a example for error')
        except Exception as e:
            log.exception(e)