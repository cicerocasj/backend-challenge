from django.db import models
from simple_history.models import HistoricalRecords
from rest.models import BaseModels
from django.db.models import F
from django.db.models.signals import m2m_changed
from rest.tags.models import Tags


ACTIVITY_TYPE_INDOORS = 'indoors'
ACTIVITY_TYPE_OUTDOORS = 'outdoors'
ACTIVITY_TYPE_CHOICES = (
    (ACTIVITY_TYPE_INDOORS, 'Indoors'),
    (ACTIVITY_TYPE_OUTDOORS, 'Outdoors'),
)

STATUS_OPEN = 'open'
STATUS_DONE = 'done'
STATUS_CHOICES = (
    (STATUS_OPEN, 'Open'),
    (STATUS_DONE, 'Done'),
)


class Tasks(BaseModels):
    Title = models.CharField(max_length=255)
    Notes = models.CharField(max_length=255)
    Priority = models.IntegerField()
    RemindMeOn = models.DateField()
    ActivityType = models.CharField(max_length=8, choices=ACTIVITY_TYPE_CHOICES, default=ACTIVITY_TYPE_INDOORS)
    Status = models.CharField(max_length=4, choices=STATUS_CHOICES, default=STATUS_OPEN)
    TaskList = models.ForeignKey('TaskList', on_delete=models.CASCADE)
    Tags = models.ManyToManyField('Tags')
    history = HistoricalRecords()

    def __str__(self):
        return self.Title


def set_tags_count(pk_set, action, **kwargs):
    value = 0
    if action == 'post_remove':
        value = -1
    if action == 'post_add':
        value = 1
    if value != 0:
        Tags.objects.filter(Id__in=pk_set).update(Count=F('Count') + value)


m2m_changed.connect(set_tags_count, sender=Tasks.Tags.through)
