from . import models
from . import serializers
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


class TasksViewSet(viewsets.ModelViewSet):
    """
    retrieve:
        Return a Tasks instance.

    list:
        Return all Tasks, ordered by most recently joined.

    create:
        Create a new Tasks.

    delete:
        Remove an existing Tasks.

    partial_update:
        Update one or more fields on an existing Tasks.

    update:
        Update a Tasks.
    """
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = models.Tasks.objects.all()
    serializer_class = serializers.TasksSerializer
