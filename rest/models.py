import uuid
from django.db import models


class BaseModels(models.Model):
    Id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)

    class Meta:
        abstract = True


from .tags.models import Tags
from .task_list.models import TaskList
from .tasks.models import Tasks
