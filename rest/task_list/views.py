from . import models
from . import serializers
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


class TaskListViewSet(viewsets.ModelViewSet):
    """
    retrieve:
        Return a TaskList instance.

    list:
        Return all TaskList, ordered by most recently joined.

    create:
        Create a new TaskList.

    delete:
        Remove an existing TaskList.

    partial_update:
        Update one or more fields on an existing TaskList.

    update:
        Update a TaskList.
    """
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = models.TaskList.objects.all()
    serializer_class = serializers.TaskListSerializer

