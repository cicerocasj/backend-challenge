from django.db import models
from simple_history.models import HistoricalRecords
from rest.models import BaseModels


class TaskList(BaseModels):
    Name = models.CharField(max_length=255)
    history = HistoricalRecords()

    def __str__(self):
        return self.Name
