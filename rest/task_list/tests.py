import json
from rest_framework.test import APIClient
from django.contrib.auth.models import User
from . import models, serializers
from rest_framework import status
from django.test import TestCase


class TaskListTests(TestCase):

    def get_token(self):
        response = self.client.post('/api-token-auth/', {'username': self.username, 'password': self.password})
        return response.json().get('token')

    def setUp(self):
        self.client = APIClient()
        self.MyModel = models.TaskList
        self.name = 'taskList'
        self.MySerializer = serializers.TaskListSerializer
        self.username = 'api'
        self.password = 'backend-challenge2020'
        self.user = User.objects.create_superuser(username=self.username, password=self.password)
        self.token = self.get_token()
        self.assertIsInstance(self.token, str, 'token as string')
        self.instance1 = self.MyModel.objects.create(Name='%s 1' % self.name)
        self.instance2 = self.MyModel.objects.create(Name='%s 2' % self.name)
        self.instance3 = self.MyModel.objects.create(Name='%s 3' % self.name)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token)

    def tearDown(self):
        User.objects.all().delete()
        self.MyModel.objects.all().delete()

    def url(self, pk=None):
        return '/%s/%s/' % (self.name, pk) if pk else '/%s/' % self.name

    def test_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION='')
        response = self.client.get(self.url())
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED, 'not authorized')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token)
        response = self.client.get(self.url())
        self.assertEqual(response.status_code, status.HTTP_200_OK, 'authorized')

    def test_get_list(self):
        response = self.client.get(self.url())
        queryset = self.MyModel.objects.all()
        serializer = self.MySerializer(queryset, many=True)
        self.assertEqual(response.data, serializer.data, 'list content')
        self.assertEqual(response.status_code, status.HTTP_200_OK, 'status code GET')

    def test_get_instance(self):
        response = self.client.get(self.url(self.instance1.pk))
        serializer = self.MySerializer(self.instance1)
        self.assertEqual(response.data, serializer.data, 'content')
        self.assertEqual(response.status_code, status.HTTP_200_OK, 'status code GET')
        invalid_pk = '1234'
        response = self.client.get(self.url(invalid_pk))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND, 'status code GET')

    def test_post(self):
        n_elements_pre = self.MyModel.objects.all().count()
        data = {
            'Name': '%s 4' % self.name
        }
        response = self.client.post(
            self.url(),
            data=json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, 'status code POST')
        n_elements_post = self.MyModel.objects.all().count()
        self.assertEqual(n_elements_pre + 1, n_elements_post, 'register count')

    def test_put(self):
        new_name = '%s 1.1' % self.name
        serializer = self.MySerializer(self.instance1)
        data = serializer.data
        data['Name'] = new_name
        response = self.client.put(
            self.url(self.instance1.pk),
            data=json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, 'status code PUT')
        self.assertEqual(response.data, data, 'content')
        updated_instance = self.MyModel.objects.get(pk=self.instance1.pk)
        self.assertEqual(updated_instance.Name, new_name, 'register field value')

    def test_delete(self):
        n_elements_pre = self.MyModel.objects.all().count()
        response = self.client.delete(
            self.url(self.instance1.pk)
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, 'status code DELETE')
        n_elements_post = self.MyModel.objects.all().count()
        self.assertEqual(n_elements_pre - 1, n_elements_post, 'register count')

    def test_patch(self):
        new_name = '%s 1.1' % self.name
        serializer = self.MySerializer(self.instance1)
        data = serializer.data
        data['Name'] = new_name
        response = self.client.put(
            self.url(self.instance1.pk),
            data=json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, 'status code PATCH')
        self.assertEqual(response.data, data, 'content')
        updated_instance = self.MyModel.objects.get(pk=self.instance1.pk)
        self.assertEqual(updated_instance.Name, new_name, 'register field value')
