from django.urls import path
from django.conf.urls import include
from rest_framework import routers
from .tags import views as tags_view
from .task_list import views as task_list_views
from .tasks import views as tasks_views


router = routers.DefaultRouter()
router.register(r'tags', tags_view.TagsViewSet, basename='tags')
router.register(r'taskList', task_list_views.TaskListViewSet, basename='taskList')
router.register(r'tasks', tasks_views.TasksViewSet, basename='tasks')


urlpatterns = [
    path('', include(router.urls)),
]
