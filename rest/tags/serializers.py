from rest_framework import serializers
from . import models


class TagsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Tags
        fields = ['Id', 'Name', 'Count']
