from django.contrib import admin
from . import models
from simple_history.admin import SimpleHistoryAdmin


admin.site.register(models.Tags, SimpleHistoryAdmin)
