from django.db import models
from simple_history.models import HistoricalRecords
from rest.models import BaseModels


class Tags(BaseModels):
    Name = models.CharField(max_length=255)
    Count = models.BigIntegerField(default=0)  # because it is better to scale in the case of a lot of data
    history = HistoricalRecords()

    def __str__(self):
        return '%s - count: %s' % (self.Name, self.Count)
