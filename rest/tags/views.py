from . import models
from . import serializers
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


class TagsViewSet(viewsets.ModelViewSet):
    """
    retrieve:
        Return a Tags instance.

    list:
        Return all Tags, ordered by most recently joined.

    create:
        Create a new Tags.

    delete:
        Remove an existing Tags.

    partial_update:
        Update one or more fields on an existing Tags.

    update:
        Update a Tags.
    """
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = models.Tags.objects.all()
    serializer_class = serializers.TagsSerializer

